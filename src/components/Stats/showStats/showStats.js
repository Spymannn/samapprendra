import React from 'react';

const showStats = ({teamStat, listStat, selectStat, isAllStatThere}) => {
    const thingToReturn = [];

    //  show all stat for a team
    if (teamStat.length > 0 ) {
        // thingToReturn.push(<ul className="list-group">);

         teamStat.forEach((stat, index) => {
         // thingToReturn.push(<p key={index}>{stat.name} | {stat.coef}</p>);
         thingToReturn.push(<li className="list-group-item" key={index}>{stat.name} | {stat.coef}</li>);

        });
        // thingToReturn.push(</ul>);
    }


    // show all stat that the team don't have and insert them as button to add them for the team
    if (!isAllStatThere)  {
         listStat.forEach((stat, index) => {
             // filter all stat that the team doesn't have
             if (teamStat.findIndex( data => stat.id === data.idStat) === -1) {
                thingToReturn.push( <button className="btn btn-secondary" key={index + 'isAllThere'} onClick={() => selectStat(stat)}>{stat.name}</button> )
             }  
        });
    }


    return thingToReturn;

    

    // if (teamStat.length > 0 ) {
    //     return teamStat.map((stat, index) => {
    //         return <p key={index}>{stat.name} | {stat.coef}</p>
    //     });
    // }
    // else {
    //     return listStat.map((stat, index) => {
    //     return( <button key={index} onClick={() => selectStat(stat)}>{stat.name}</button> )
    //     });
    // }
}


export default showStats;