import React, {Component} from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

class AddStat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            coef: 0,
            lastId: null,
            previousStat: {
                id: null,
                name: null
            }
        }
    }

    // change the coef for stat to add
    changeCoefHandler(e) {
        e.preventDefault();
        this.setState({coef: e.target.value});
    }

    componentDidMount()  {
        this.getLastId();
    }

    // get last id of the DB json from team stats for all teams
    getLastId = () => {
        axios.get('http://localhost:5000/teamstats')
            .then( success => {
                        const lastId = success.data.pop().id;
                        this.setState({lastId: lastId});
                }, error => {
                    console.log(error);
                }
            );
    }

    // add stat to DB json for a team
    addStatHandler = (stat, coef) => {
        if (this.state.lastId) {
            const stat = {
                id: this.state.lastId + 1,
                idTeam: this.props.teamId,
                idStat: this.props.stat.id,
                coef: this.state.coef,
                isBonus: false,
                multiplier: 1

            }
            axios.post('http://localhost:5000/teamstats', stat)
            .then( success => {
                        console.log('success data inserted correctly', success);
                        this.getLastId();
                        this.props.getStatTeam();
                        this.setState({coef: 0});
                        this.setState({previousStat: this.props.stat});
                }, error => {
                    console.log(error);
                }
            );
        } 
    }
    
    render() {
        return (
            <div className="input-group mb-3">
                <h3>
                    <small className="text-muted">Add the coef for your stat { (this.props.stat.name === this.state.previousStat.name ) ? null : this.props.stat.name}</small>
                </h3>
                <input
                    type='number' 
                    value={this.state.coef}
                    onChange={(e) => this.changeCoefHandler(e)}
                    className="form-control"
                />
                <div className="input-group-append">
                <button className="btn btn-outline-secondary" onClick={() => this.addStatHandler(this.props.stat, this.state.coef)} disabled={!this.props.stat.name ||
                     (this.props.stat.name === this.state.previousStat.name )} > Add stat </button>
                </div>
            </div>
        );
    }

}


AddStat.propTypes = {
    coef: PropTypes.number,
    addStat: PropTypes.func,
    statSelected: PropTypes.bool,
    stat: PropTypes.object,
    teamId: PropTypes.number,
    getStatTeam: PropTypes.func
}

export default AddStat;