import React, {Component}from 'react'
import PropTypes from 'prop-types';
import axios from 'axios';

import './Team.css';

// Component 
import ShowStats from './../../Stats/showStats/showStats';
import AddStat from './../../Stats/addStat/addStat';



// Context 

class Team extends Component {
    constructor (props) {
        super(props);
        this.state = {
            teamStat: [],
            statsName: [],
            showStats: false,
            statToAdd: {
                id: null,
                name: null
            },
            isAllStatThere: false
        }
    }

    componentDidMount()  {
        this.getStatsTeamHandler(this.props.idTeam);
    }

    // get all stat for a team (if there is stat)
    getStatsTeamHandler = (teamId) => {
        this.setState({teamStat:[]});
        axios.get('http://localhost:5000/teamstats?idTeam='+teamId)
            .then( success => {
                    if (success.data.length > 0) {
                        success.data.forEach( stat => {
                            this.getStatsName(stat);
                        });
                        if (success.data.length === 9) {
                            this.setState({isAllStatThere: true});
                        }
                    } 
                }, error => {
                    console.log(error);
                }
            );
    }

    // Get all name of statistics
    getStatsName = (stats) => {
        axios.get('http://localhost:5000/stats/'+stats.idStat)
            .then(
                success => {
                    const teamStatLine = {...stats, name: success.data.name};
                    const statArrayComplete = [...this.state.teamStat, teamStatLine];
                    this.setState({teamStat: statArrayComplete});
                }, error => {
                    console.log('error: ', error);
                }
            )
    }   

    // update if we want to show or not the statistic for a team
    updateShowStats = () => {
        this.setState({showStats: !this.state.showStats});
    }

    // select stat from button on showStats.js and send it addStat later on the JSX
    selectStat = (stat) => {
        this.setState({statToAdd: stat});
    }

    render() {     
        const { teamStat } = this.state; // deconstruction JS ES6 

        return  (
            <div className="card cardTeam">
                <div className="card-header">
                    {this.props.name} 
                </div>
                <div className="card-body">
                    {/* <a className="badge badge-danger buttonToLeft" onClick={this.props.action}>Delete</a> */}
                    <button type="button" className="close" aria-label="Close" onClick={this.props.action}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <a className="badge badge-success buttonToLeft" onClick={this.props.action}>Choose</a>

                    <h5 className="card-title">{this.props.city} </h5>
                    <button className="btn btn-outline-info"  onClick={() => this.updateShowStats()}> {this.state.showStats ? 'Hide statistics': 'Show statistics'} </button>
                    <br />

                    <div className="card-text">
                    {
                        (this.state.showStats ? <ShowStats teamStat={teamStat} listStat={this.props.listStat} selectStat={this.selectStat.bind(this)}
                            isAllStatThere={this.state.isAllStatThere}/> : null)
                    }
                    {
                        (this.state.teamStat.length < 9 && this.state.showStats && this.state.statToAdd ? 
                            <AddStat stat={this.state.statToAdd} teamId={this.props.idTeam} getStatTeam={() => this.getStatsTeamHandler(this.props.idTeam) }/> : 
                            null)
                    }
                    </div>
                    <br />
                </div>
            </div>
        )}
}

Team.propTypes = {
    name: PropTypes.string,
    city: PropTypes.string,
    action: PropTypes.func,
    nameChanged: PropTypes.func,
    position: PropTypes.number,
    idTeam: PropTypes.number,
    listStat: PropTypes.array
}

// const team = (props) => {
//     return (
//         <div className='Team' >
//             <p>Team: {props.name} | city: {props.city}</p>    <a onClick={props.action}>X</a>
//             <input type='text' onChange={props.nameChanged} value={props.name}/>
//         </div>
//     );
// }

export default Team;