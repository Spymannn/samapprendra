import React from 'react';
import PropTypes from 'prop-types'
import './Cockpit.css';

// import from Redux
import { connect } from 'react-redux';


const cockpit = (props) => (
    <div className="CockpitCenter">
        <button className="btn btn-outline-info" onClick={props.toggleTeam} disabled={!props.isConnectedToDB || !props.isUserConnected}>
            {props.isTeamShowed? 'Disable list':'Enable list'}
        </button>    
    </div> 
);

cockpit.propTypes = {
    toggleTeam: PropTypes.func,
    isTeamShowed: PropTypes.bool,
    isConnectedToDB: PropTypes.bool
}

const mapStateToProps = (state) => {
    return {
        isUserConnected: state.isConnected
    }
}

export default connect(mapStateToProps)(cockpit);
