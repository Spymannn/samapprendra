import { FETCH_GIT_TIPS, FETCH_JS_TIPS } from "../actions/types";

export default (state = {}, action) => {
  console.warn("action type", action);
  switch (action.type) {
    case FETCH_GIT_TIPS:
      return  {
        ...state,
        gitTipsList: action.payload
      };
    case FETCH_JS_TIPS: 
      return {
        ...state,
        jsTipsList: action.payload
      };
    default:
      return state;
  }
};