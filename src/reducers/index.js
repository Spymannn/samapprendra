import { combineReducers } from "redux";

import astucesList from "./dataReducer";

export default combineReducers({
  astucesList
});