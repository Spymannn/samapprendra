import { gitTipsRef, jsTipsRef } from "../config/firebase";
import { FETCH_GIT_TIPS, FETCH_JS_TIPS } from "./types";

// export const addToDo = newToDo => async dispatch => {
//   gitTipsRef.push().set(newToDo);
// };

// export const completeToDo = completeToDoId => async dispatch => {
//   gitTipsRef.child(completeToDoId).remove();
// };

export const fetchGitTips = () => async dispatch => {
    console.warn("fetchGitTips");
  gitTipsRef.on("value", snapshot => {
    dispatch({
      type: FETCH_GIT_TIPS,
      payload: snapshot.val()
    });
  });
};

export const fetchJsTips = () => async dispatch => {
    console.warn("fetchJsTips");
  jsTipsRef.on("value", snapshot => {
    dispatch({
      type: FETCH_JS_TIPS,
      payload: snapshot.val()
    });
  });
};