import React from 'react';

// router import
import { Link } from 'react-router-dom';

import PropTypes from 'prop-types';

// redux
import { connect } from 'react-redux';

import './menu.css';

// antd
import { Menu, Icon, Button, Input } from 'antd';

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

const menu = (props) => {
    return (
        <Menu
            onClick={this.handleClick}
            mode="horizontal"
            className="centerMenu"
        >
            {/* { !props.isUserConnected ? 
                <Menu.Item key="login">
                    <Link to={`/login`} className="active"><Icon type="login" />Login</Link>
                </Menu.Item>
            : 
                <Menu.Item key="logout">
                    <Button type="danger" onClick={ (e) => props.logoutHandler(e)}><Icon type="login" />Logout</Button>
                </Menu.Item>
            }
            <Menu.Item key="home">
                <Link to={`/home`} className="active"><Icon type="home" />Home</Link>
            </Menu.Item> */}
            <SubMenu title={<Link to={`/astuces`} className="active"><Icon type="code" />Astuces</Link>}>
            <MenuItemGroup title="Astuces">
                <Menu.Item key="setting:1">
                    <Link to={`/astuces/git`} className="active"><Icon type="fork" />Git</Link>
                </Menu.Item>
                <Menu.Item key="setting:2">
                    <Link to={`/astuces/javascript`} className="active"><Icon type="robot" />Javascript</Link>
                </Menu.Item>
            </MenuItemGroup>
            <MenuItemGroup title="Frameworks">
                <Menu.Item key="setting:3">
                    <Link to={`/astuces/react`} className="active"><Icon type="cluster" />React</Link>
                </Menu.Item>
                <Menu.Item key="setting:4">
                    <Link to={`/astuces/angular`} className="active"><Icon type="block" />Angular</Link>
                </Menu.Item>
            </MenuItemGroup>
            </SubMenu>
            <Menu.Item key="faq">
                <Link to={`/faq`} className="active"><Icon type="question-circle" />F.A.Q.</Link>
            </Menu.Item>
            <Menu.Item key="about">
                <Link to={`/about`} className="active"><Icon type="security-scan" />À propos</Link>
            </Menu.Item>
            <Menu.Item key="search">
                <Input.Search
                    placeholder="Rechercher une astuce"
                    onSearch={value => console.log(value)}
                    style={{ width: 250 }}
                    enterButton
                    />
            </Menu.Item>
            
        </Menu>
    )
}

menu.propTypes = {
    logoutHandler: PropTypes.func
}

const mapStateToProps = (state) => {
    return {
        isUserConnected: state.isConnected
    }
}

export default connect(mapStateToProps)(menu);