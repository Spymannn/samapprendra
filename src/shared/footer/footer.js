import React, {Component} from 'react';

import './footer.css';
import { Row, Col } from 'antd';
import { Link } from 'react-router-dom';
import { Timeline } from 'react-twitter-widgets'


class Footer extends Component {
    render () {
        return (
            <footer className="App-footer">
                <div className="gutter-example">
                    <Row gutter={16}>
                        <Col className="gutter-row" span={4}>
                            <div className="gutter-box"></div>
                        </Col>
                        <Col className="gutter-row" span={6}>
                            <div className="gutter-box">
                                <Timeline
                                    dataSource={{
                                        sourceType: 'profile',
                                        screenName: 'Spymannn'
                                    }}
                                    options={{
                                        username: 'Spymannn',
                                        height: '200'
                                    }}
                                    onLoad={() => console.log('Timeline is loaded!')}
                                />
                            </div> 
                        </Col>
                        <Col className="gutter-row" span={4}>
                            <div className="gutter-box">
                                    <h3 className="titleFooter">Contact</h3>
                                    <a className="linkFooter" href="mailto:hanini.samir@gmail.com" target="_top" rel="noopener noreferrer">hanini.samir@gmail.com</a><br />
                            </div>
                        </Col>
                        <Col className="gutter-row" span={3}>
                            <div className="gutter-box">
                                <Link className="linkFooter" to={`/astuces`} >Astuces </Link> <br />
                                <Link className="linkFooter" to={`/astuces/git`} >Git</Link> <br />
                                <Link className="linkFooter" to={`/astuces/javascript`} >Javascript</Link> <br />
                                <Link className="linkFooter" to={`/astuces/react`} >React</Link> <br />
                                <Link className="linkFooter" to={`/astuces/angular`} >Angular</Link> <br />
                                <Link className="linkFooter" to={`/faq`} >F.A.Q.</Link> <br />
                                <Link className="linkFooter" to={`/about`} >À propos</Link> <br />
                            </div>
                        </Col>
                        <Col className="gutter-row" span={3}>
                            <div className="gutter-box">
                                <a className="linkFooter" href="https://github.com/Spyman15" target="_blank" rel="noopener noreferrer">Github</a><br />
                                <a className="linkFooter" href="https://www.linkedin.com/in/samir-hanini-0197a784/" target="_blank" rel="noopener noreferrer">Linkedin</a><br />
                                <a className="linkFooter" href="https://medium.com/@Spymannn" target="_blank" rel="noopener noreferrer">Medium</a><br />
                                <a className="linkFooter" href="https://www.facebook.com/samir.hanini.3" target="_blank" rel="noopener noreferrer">Facebook</a><br />
                                <a className="linkFooter" href="https://twitter.com/Spymannn" target="_blank" rel="noopener noreferrer">Twitter</a><br />
                                <a className="linkFooter" href="https://www.instagram.com/samhn_" target="_blank" rel="noopener noreferrer">Instagram</a><br />    
                            </div>
                        </Col>
                        <Col className="gutter-row" span={4}>
                            <div className="gutter-box">
                            
                            </div>
                        </Col>
                    </Row>
                    <div className="copyright">
                        <br /> <br />
                        <span>© Copyright 2018 - Samir Hanini</span><br />
                        <span>All rights reserved.</span>
                        <br /><br />
                    </div>
                    

                </div>
            </footer>
        )
    }
}



export default Footer;