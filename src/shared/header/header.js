import React, {Component} from 'react';

import './header.css';

class Header extends Component{
    render() {
        return (
            <header className="App-header">
                <h1 className="App-title">Sam'apprend(s) des choses!</h1>
            </header>
        )
    }
}

export default Header;