import React, { Component } from 'react';
import './TeamListPage.css';

import axios from 'axios';
// Components
import Teams from './../../components/Teams/Teams';
import Cockpit from './../../components/Cockpit/Cockpit';

import { connect } from 'react-redux';


class TeamListPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      teams: [],
      showTeams: false,
      isConnectedToDB: true
    }
  }

  componentDidMount() {
    axios.get('http://localhost:5000/teams')
      .then( success => {
        this.setState({teams: success.data});
        this.setState({isConnectedToDB: true});
        }, error => {
          this.setState({isConnectedToDB: false});
          console.log(error);
        }
      );
  }


  nameChangedHandler = (event, id) => {
    event.preventDefault();
    const teamIndex = this.state.teams.findIndex(t => t.id === id );
    const team = {...this.state.teams[teamIndex]};
    team.name = event.target.value;

    const teams = [...this.state.teams];
    teams[teamIndex] = team;

    this.setState({teams: teams});
  } 

  deleteTeamHandler = (indexTeam) => {
    const teams = [...this.state.teams];
    teams.splice(indexTeam, 1);
    this.setState({teams: teams});
  }

  toggleTeamsHandler = () => {
    this.setState({showTeams: !this.state.showTeams});
  }

  componentDidUpdate() {}
 
  render() {   
    let teams = null;
    if (this.state.showTeams && this.props.isUserConnected) {
       teams = 
          <Teams teams={this.state.teams} 
            action={this.deleteTeamHandler}
            nameChanged={this.nameChangedHandler}
          />
    }

    let noConnectedToDB = <p>No connexion with the database </p>;

    return (
      <div className="TeamListPage" id = "teamsTotal"
      >
        <Cockpit 
          isTeamShowed={this.state.showTeams}
          toggleTeam={this.toggleTeamsHandler}
          isConnectedToDB = {this.state.isConnectedToDB}
        />
        { (this.state.isConnectedToDB ? teams : noConnectedToDB )} 
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isUserConnected: state.isConnected
  }
} 

export default connect(mapStateToProps)(TeamListPage);