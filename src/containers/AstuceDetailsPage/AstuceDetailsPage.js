import React, {Component} from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Layout, Breadcrumb, Divider } from 'antd';

import './AstuceDetailsPage.css';



import Parser from 'html-react-parser';

const astuce = {
    title: 'Astuce git #1: Comment modifier le nom d\'une branche?',
    details: '<p>Yo, <br /> Comment je fais pour modifier le nom de ma branche ? Une petite erreur de nom ca peut arriver non ? <br /><br />' +
    'Si je suis à l’intérieur de ma branche:<br />' + 
    '<code>git branch -m nouveau_nom_de_branch</code> <br />' + 
    'Si je suis à partir d’une autre branche: <br />' + 
    '<code>git branch -m ancien_nom_de_branch  nouveau_nom_de_branch</code> <br /><br />' +
    'Ensuite, si la branche avait été push, il faut supprimer l\’ancien nom et renommer <br />' +
    '<code>git push origin  :ancien_nom_de_branch  nouveau_nom_de_branch</code> <br />' +
    ' Pusher sur la nouvelle branche pour reset et lier le nouveau nom avec la branche en local: <br />' +
    ' <code>git push origin -u nouveau_nom_de_branch</code><br /><br />' +
    ' et voilà :)<br />'
}

class AstuceDetailsPage extends Component {
    constructor(props) {
        super(props);
        console.warn('props ', this.props.match.params.id);
    }
   
    render() {
        return (
            <Layout style={{ padding: '0 24px 24px', background: '#fff' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>Astuces</Breadcrumb.Item>
                    {(this.props.page ? <Breadcrumb.Item>{this.props.page}</Breadcrumb.Item> : null)}
                    <Breadcrumb.Item>{astuce.title}</Breadcrumb.Item>
                </Breadcrumb>
                <Layout.Content style={{ background: '#fff', padding: '0 300px', margin: 0, minHeight: 280 }} className="textAstuce">
                    <Divider><h5>{astuce.title}</h5></Divider><br />
                    {Parser(astuce.details) } 
                </Layout.Content>
            </Layout> 
           
        );
    }

}


const mapStateToProps = (state) => {
    return {
    }
}

export default connect(mapStateToProps)(withRouter(AstuceDetailsPage));


{/* <p>Yo, <br /> Comment je fais pour modifier le nom de ma branche ? Une petite erreur de nom ca peut arriver non ? <br /><br />Si je suis à l’intérieur de ma branche:<br /><code>git branch -m nouveau_nom_de_branch</code> <br />Si je suis à partir d’une autre branche: <br /><code>git branch -m ancien_nom_de_branch  nouveau_nom_de_branch</code> <br /><br />Ensuite, si la branche avait été push, il faut supprimer l\’ancien nom et renommer <br /><code>git push origin  :ancien_nom_de_branch  nouveau_nom_de_branch</code> <br />Pusher sur la nouvelle branche pour reset et lier le nouveau nom avec la branche en local: <br /><code>git push origin -u nouveau_nom_de_branch</code><br /><br /> et voilà :)<br /> */}