import React, { Component } from 'react';
import PropTypes from 'prop-types'

import { connect } from 'react-redux';

import { connectUser } from './../../actions/userActions';
import './LoginPage.css';


import axios from 'axios';

import { Layout, Breadcrumb, Form, Icon, Input, Button, Checkbox } from 'antd';


class LoginPage extends Component {

    connect = (e, email, password) => {
        e.preventDefault();
        this.props.connectUser();
        // all of that has to be done in the backend side of course and in a better way !!!
        // all of that is just a test to improve my skills using React + Redux and Axios

        // axios.get('http://localhost:5000/users?email=' + email)
        //   .then( success => {
        //       if (success.data.length ===  0) {
        //         console.log('User not found');
        //       } else {
        //         const user = success.data.pop()
        //         if (user.password === password) {
        //             console.log('Ok, user connected', user);
        //             this.props.connectUser();
        //             this.props.history.push('/home');
        //         } else {
        //           console.log('Incorrect password');
        //         }
        //       } 
        //     }, error => {
        //       console.log('Error connection to users list: ',error);
        //     }
        //   );
    
      }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
            console.log('Received values of form: ', values);
            }
        });
    }

    render () {
        const { getFieldDecorator } = this.props.form;

        return (
            <Layout style={{ padding: '0 24px 24px', background: '#fff' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>Login</Breadcrumb.Item>
                </Breadcrumb>
                <Layout.Content style={{ background: '#fff', padding: 24, margin: 0, minHeight: 280 }}>
                    {/* <div className="container">
                        <form onSubmit={ (e) => this.connect(e, this.refs.email.value, this.refs.password.value) }>
                            <div className="form-group">
                                <label >Email address</label>
                                <input type="email" ref="email" className="form-control" placeholder="Enter email" />
                                <small className="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div className="form-group">
                                <label >Password</label>
                                <input type="password" ref="password" className="form-control" placeholder="Password"/>
                            </div>
                            <button className="btn btn-primary">Submit</button>
                        </form>  
                    </div>  */}
                    <div className="container">
                        <Form onSubmit={this.handleSubmit} className="login-form">
                            <Form.Item>
                            {getFieldDecorator('userName', {
                                rules: [{ required: true, message: 'Please input your username!' }],
                            })(
                                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                            )}
                            </Form.Item>
                            <Form.Item>
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: 'Please input your Password!' }],
                            })(
                                <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                            )}
                            </Form.Item>
                            <Form.Item>
                            <Button type="primary" htmlType="submit" className="login-form-button">
                                Log in
                            </Button>
                             Or <a href=""> register now!</a>
                            </Form.Item>
                            {getFieldDecorator('remember', {
                                valuePropName: 'checked',
                                initialValue: true,
                            })(
                                <Checkbox>Remember me</Checkbox>
                            )}
                            <Form.Item>
                                <a className="login-form-forgot" href="">Forgot password</a>
                            </Form.Item>
                        </Form>
                    </div>
                </Layout.Content>
            </Layout>
        );
    }
}

LoginPage.propTypes = {
    connectHandler: PropTypes.func
}

const mapStateToProps = (state) => {
    return {
        isUserConnected: state.isConnected
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        connectUser: () => { dispatch(connectUser()) }
    }
}

const LoginForm = Form.create({})(LoginPage);
export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);