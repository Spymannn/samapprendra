import React, {Component} from 'react';
import { Link } from 'react-router-dom';

import { Layout, Breadcrumb } from 'antd';

import underConstuction404 from './../../assets/underconstruction4-Spyman.png';

class NotFoundPage extends Component {
    render() {
        return (
            <Layout style={{ padding: '0 50px 50px', background: '#fff' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>Oups! Page non trouvée</Breadcrumb.Item>
                </Breadcrumb>
                <Layout.Content style={{ background: '#fff', padding: '0 350px', margin: 0, minHeight: 280 }}>
                <Link to={`/astuces`}>Retourner vers la page Astuces</Link>
                    <img src={underConstuction404} alt="404 under construction"/>
                </Layout.Content>
            </Layout>
        )
    }
}

export default NotFoundPage;