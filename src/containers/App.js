import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

// Components
import Menu from './../shared/menu/menu';

import { Route, Switch , BrowserRouter} from "react-router-dom";

import HomePage from './../containers/HomePage/HomePage';
import LoginPage from './../containers/LoginPage/LoginPage';
import AstucePage from './../containers/AstucePage/AstucePage';
import AboutPage from './../containers/AboutPage/AboutPage';
import NotFoundPage from './../containers/NotFoundPage/NotFoundPage';
import FaqPage from './../containers/FaqPage/FaqPage';
import AstucePageDetails from './../containers/AstuceDetailsPage/AstuceDetailsPage';

// header + footer
import Header from './../shared/header/header';
import Footer from './../shared/footer/footer';

// ga 
import ReactGA from 'react-ga';

// antd
import { Button, notification } from 'antd';


// import redux
import { connect } from 'react-redux';

class App extends Component {

  componentDidMount(props) {
    this.initializeReactGA();
    if (!localStorage.getItem('areCookieAccepted')) {
      const key = `open${Date.now()}`;
      const btn = (
        <Button type="secondary" size="default" onClick={() =>{ this.acceptCookie(); notification.close(key);}}>
          Confirm
        </Button>
      );
      notification.info({
        message: 'Bienvenue',
        description: 'Bienvenue sur Sam\'apprends des choses.',
        duration: 0,
        btn,
        key,
      });
    } 
  }

  acceptCookie = () => {
    localStorage.setItem('areCookieAccepted', true);
  }

  initializeReactGA = () => {
    ReactGA.initialize('UA-128324503-1');
    ReactGA.pageview('/');
    ReactGA.pageview('/home');
    ReactGA.pageview('/astuces');
    ReactGA.pageview('/about');

  }

  logoutHandler(e) {
    e.preventDefault();
    this.props.disconnectUser();
  }

  render() {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    return (
      <BrowserRouter>
        <content className="mainBody" >
            <Header />
            <Menu logoutHandler={this.logoutHandler.bind(this)}/>
            <Switch>
              <Route path='/' exact component={AstucePage} />
              <Route path='/home' exact component={NotFoundPage} /> {/* remettre HomePage */}
              <Route 
                path='/login' exact 
                component={NotFoundPage} /> {/* remettre LoginPage */}
              <Route path='/astuces' exact render={() => <AstucePage />}/>
              <Route path='/astuces/git' exact render={() => <AstucePage page={"git"} />}/>
              <Route path='/astuces/git/:id' exact render={() => <AstucePageDetails page={"git"} />}/>
              <Route path='/astuces/javascript' exact render={() => <AstucePage page={"javascript"} />}/>
              <Route path='/astuces/react' exact render={() => <AstucePage page={"react"} />}/>
              <Route path='/astuces/angular' exact render={() => <AstucePage page={"angular"} />}/>
              <Route path='/about' exact render={() => <AboutPage />}/>
              <Route path='/faq' exact render={() => <FaqPage />}/>
              <Route component={NotFoundPage} />
            </Switch> 
            <Footer />
        </content> 
        </BrowserRouter>     
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isUserConnected: state.isConnected
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    disconnectUser: () => { dispatch({type: 'CONNECT_USER'}) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
