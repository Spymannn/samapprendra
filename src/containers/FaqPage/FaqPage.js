import React, {Component} from 'react';
import { Link } from 'react-router-dom';

import { Layout, Breadcrumb, Divider } from 'antd';


class FaqPage extends Component {
    render() {
        return (
            <Layout style={{ padding: '0 50px 50px', background: '#fff' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>F.A.Q.</Breadcrumb.Item>
                </Breadcrumb>
                <Layout.Content style={{ background: '#fff', padding: '0 300px', margin: 0, minHeight: 280 }}>

                <Divider><h3>F.A.Q.</h3></Divider><br />
                <Divider orientation="left" dashed="true">Pourquoi ce site web?</Divider>
                <p>Je partageais mes tips Git avec certains de mes ami(e)s et l'idée est venue, pourquoi ne pas faire un site web
                qui repertorierait tout ca? Tout simplement.</p>
                <Divider orientation="left" dashed="true">Pourquoi <cite>Sam'apprends des choses</cite>?</Divider>
                <p>
                    Eh bien, je m'appelle Samir &rarr; <code>Sam</code> <br />
                    J'espère apprendre des choses des autres et apprendre aux autres avec mes tips &rarr; <code>apprends</code> <br />
                    et puis ca peut aller du contenu concernant Git, Javascript ou d'autres languages ou framework &rarr; <code>des choses</code> <br />
                    Tout simplement.
                </p>
                <Divider orientation="left" dashed="true">Quel est ton métier?</Divider>
                <p>Je suis développeur principalement sur les technologies Javascript avec les frameworks React ou Angular 
                pour le front et du NodeJS/Express pour le back.</p>
                <Divider orientation="left" dashed="true">Pourra-t-on participer et ajouter des astuces?</Divider>
                <p>Bien sûr, c'est dans les plans, le but et de faire participer les gens et de mettre en avant
                le partage d'astuces, de tips.</p>
                <Divider orientation="left" dashed="true">Es-tu spécialiste dans tous les domaines pour lesquels tu donnes tes astuces?</Divider>
                <p><code>ABSOLUMENT PAS</code>, je ne suis pas du tout un spécialiste et je ne prétend pas être au top
                mais j'ai pu trouvé au cours de mon expérience certaines astuces que j'aime partager.
                Et j'ai bien hâte d'apprendre de nouvelles astuces de la part des visiteurs de ce site.</p>
                <Divider orientation="left" dashed="true">Y aura-t-il d'autres types d'astuces que les languages/frameworks ou outils déjà 
                présents?</Divider>
                <p>Si je trouve des astuces concernant d'autres languages, pourquoi pas. <br />
                Cela serait encore mieux que d'autres personnes proposent de nouvelles catégories avec des astuces. <br />
                Encore une fois, le but est de partager ici des astuces et informations afin de tous s'entraider.
                </p>
                <Divider orientation="left" dashed="true">Comment s'inscrire?</Divider>
                <p>Il suffit de cliquer sur ce <Link to={`/login`} className="active">lien</Link>.</p>
                <Divider orientation="left" dashed="true">Comment devenir créateur d'astuces?</Divider>
                <p>Lors de l'inscription, sur ce lien, il faudra faire la demande afin de devenir un créateur d'astuces. <br />
                Les astuces seront alors validées par moi avant d'être mises en ligne.
                </p>
                <Divider orientation="left" dashed="true">Le contenu sera-t-il disponible dans une autre langue?</Divider>
                <p>Oui, dans une prochaine mise à jour du site.</p>
                 {/*<Divider orientation="left" dashed="true"></Divider>
                <p></p>
                <Divider orientation="left" dashed="true"></Divider>
                <p></p>
                <Divider orientation="left" dashed="true"></Divider>
                <p></p>
                <Divider orientation="left" dashed="true"></Divider>
                <p></p>
                <Divider orientation="left" dashed="true"></Divider>
                <p></p>
                <Divider orientation="left" dashed="true"></Divider>
                <p></p>
                <Divider orientation="left" dashed="true"></Divider>
                <p></p> */}
                </Layout.Content>
            </Layout>
        )
    }
}

export default FaqPage;