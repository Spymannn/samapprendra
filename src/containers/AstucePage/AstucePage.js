import React, { Component } from 'react';

import { List, Layout, Breadcrumb , Card, Col, Row, Button } from 'antd';

import { Link } from 'react-router-dom';

import { connect } from 'react-redux';

import git from './../../assets/tips/git.png';
import js from './../../assets/tips/js.png';
import react from './../../assets/tips/react.png';
import angular from './../../assets/tips/angular.png';

import { fetchGitTips, fetchJsTips } from './../../actions/index';

const data = [
    { id: 1, title: 'Astuce git #1: Comment modifier le nom d\'une branche?'},
    { id: 2, title: 'Astuce git #2: Comment voir le dernier commit ?'},
    { id: 3, title: 'Astuce git #3: Comment ajouter un fichier oublié à mon dernier commit?'},
    { id: 4, title: 'Astuce git #4: Comment créer des alias Git ?'},
    { id: 5, title: 'Astuce git #5: Oh damn! j\'ai mal écris mon message de commit, comment l\'éditer avant de push?'},
  ];

  const dataJS = [
    { id: 1, title: 'Astuce JavaScript #1: Comment créer un array ?'},
    { id: 2, title: 'Astuce JavaScript #2: Comment récupèrer le dernier élément d\'un tableau?'},
    { id: 3, title: 'Astuce JavaScript #3: Qu\'est-ce que le .map?'},
    { id: 4, title: 'Astuce JavaScript #4: Comment filtrer un tableau?'},
    { id: 5, title: 'Astuce JavaScript #5: Oh damn! comment créer une nouvelle instance de mon objet?'},
  ];


class AstucePage extends Component {
    constructor(props) {
        super(props);
        console.warn("props", this.props.page);
    }

    componentDidMount = () => {
        this.props.fetchGitTips();
        this.props.fetchJsTips();
    }

    showBaseAstucePage = () => {
        return (
            <div style={{ padding: '5%' }}>
                <Row gutter={16}>
                <Col span={6}>
                    <Link to={`/astuces/git`}>
                        <Card
                            hoverable
                            style={{ }}
                            cover={<img alt="example" src={git} />}
                        >
                            <Card.Meta
                            title="Git"
                            description="Astuce Git"
                            />
                        </Card>
                    </Link>
                </Col>
                <Col span={6}>
                    <Link to={`/astuces/javascript`}>
                        <Card
                            hoverable
                            style={{ }}
                            cover={<img alt="example" src={js} />}
                        >
                            <Card.Meta
                            title="JavaScript"
                            description="Astuce JavaScript"
                            />
                        </Card>
                    </Link>
                </Col>
                <Col span={6}>
                    <Link to={`/astuces/react`}>
                        <Card
                            hoverable
                            style={{ }}
                            cover={<img alt="example" src={react} />}
                        >
                            <Card.Meta
                            title="React"
                            description="Astuce React"
                            />
                        </Card>
                    </Link>
                </Col>
                <Col span={6}>
                    <Link to={`/astuces/angular`}>
                        <Card
                            hoverable
                            style={{ }}
                            cover={<img alt="example" src={angular} />}
                        >
                            <Card.Meta
                            title="Angular"
                            description="Astuce Angular"
                            />
                        </Card>
                    </Link>
                </Col>
                </Row>
            </div>
        )
    }
    
    showGitAstuce = (id) => {
        return (
            <div>
                <h3 style={{ marginBottom: 16 }}>Liste Astuce Git</h3>
                <List
                header={null}
                footer={null}
                bordered
                dataSource={data}
                renderItem={item => (<List.Item> <Link to={`/astuces/git/` + item.id} className="active">{item.title}</Link></List.Item>)}
                />
            </div>
        )
    }

    showJSAstuce = (id) => {
        return (
            <div>
                <h3 style={{ marginBottom: 16 }}>Liste Astuce Javascript</h3>
                <List
                header={null}
                footer={null}
                bordered
                dataSource={dataJS}
                renderItem={item => (<List.Item> <Link to={`/astuces/javascript/` + item.id} className="active">{item.title}</Link></List.Item>)}
                />
            </div>
        )
    }

    getTips = () => {
        console.warn('tips', this.props.astucesList);
    }

    render() {
        let tipToShow = null;

        switch(this.props.page) {
            case 'git': 
                tipToShow = this.showGitAstuce();
                break;
            case 'javascript': 
                tipToShow = this.showJSAstuce();
                break;
            case 'react': 
                tipToShow = "react";
                break;
            case 'angular': 
                tipToShow = "angular";
                break;
            default:
                tipToShow = this.showBaseAstucePage();
                break;
        } 
            
        return (
            <Layout style={{ padding: '0 24px 24px', background: '#fff' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>Astuces</Breadcrumb.Item>
                    {(this.props.page ? <Breadcrumb.Item>{this.props.page}</Breadcrumb.Item> : null)}
                </Breadcrumb>
                <Layout.Content style={{ background: '#fff', padding: '0 300px', margin: 0, minHeight: 280 }}>
                    {tipToShow}
                    <Button type="danger" onClick={this.getTips}>Logout</Button>
                </Layout.Content>
            </Layout>  
        );
    }
}

const mapStateToProps = (state) => {
    console.warn('state', state);
    return {
        astucesList: state.astucesList
    }
}
  
const mapDispatchToProps = (dispatch) => {
    return {
        fetchGitTips: () => { dispatch(fetchGitTips()) },
        fetchJsTips: () => { dispatch(fetchJsTips())}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AstucePage);