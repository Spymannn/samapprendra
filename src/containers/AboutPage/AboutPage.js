import React, {Component} from 'react';

import { Layout, Breadcrumb, Avatar, Divider } from 'antd';
import { Link } from 'react-router-dom';

import avatar from './../../assets/avatar/photopro.png';

class AboutPage extends Component {
    render() {
        return (
            <Layout style={{ padding: '0 50px 50px', background: '#fff' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>À propos</Breadcrumb.Item>
                </Breadcrumb>
                <Layout.Content style={{ background: '#fff', padding: '0 300px', margin: 0, minHeight: 280 }}>
                <Avatar size={64} src={avatar} style={{margin: 24}}>
                    {'Samir'}
                </Avatar>
                <p>
                    En tant que développeur, ce que j'aime c'est trouver ou partager des petites astuces. Et j'ai un petit paquet d'astuces en stock
                    sur différents sujets. <br />
                    Et c'est pourquoi ce site.  <br />
                    Il y aura des astuces Git, JavaScript, React, Angular, ...  <br />
                    Ce site me permet également de m'exercer dans des technologies que je ne connais pas et me permet de tester de nouvelles choses. <br />
                    Le but n'étant pas de réaliser un site de tutoriels, mais uniquement d'astuces, de tips, à savoir que c'est un plus
                    avec des tutoriels que l'on peut trouver sur internet, cela permet d'aller plus loin mais c'est en aucun cas un site internet
                    de tutos. <br />
                    Bonne visite et bon tips.
                </p>
                <br />
                <Divider><h4>Contact</h4></Divider><br />
                <p>
                    Si vous avez des questions, n'hésitez pas à aller voir la <Link className="linkContact" to={`/faq`} >F.A.Q.</Link><br />
                    Si vous ne trouvez pas vos réponses, envoyez-moi un email à l'adresse : <br />
                    <a className="linkContact" href="mailto:hanini.samir@gmail.com" target="_top" rel="noopener noreferrer">hanini.samir@gmail.com</a><br />
                </p>
                </Layout.Content>
            </Layout>
        )
    }
}

export default AboutPage;