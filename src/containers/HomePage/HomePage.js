import React, { Component } from 'react';
import { Layout, Breadcrumb } from 'antd';

class HomePage extends Component {
    render() {
        return (
            <Layout style={{ padding: '0 24px 24px', background: '#fff' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>Home</Breadcrumb.Item>
                </Breadcrumb>
                <Layout.Content style={{ background: '#fff', padding: 24, margin: 0, minHeight: 280 }}>
                    Home content
                </Layout.Content>
            </Layout>
            
        );
    }
}

export default HomePage;